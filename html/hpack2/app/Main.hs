{-# LANGUAGE OverloadedStrings #-}

--import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
--import qualified Data.Text.Lazy as L
import           Lucid
import           System.Environment (getArgs, getProgName)

myPage:: [T.Text] -> Html ()
myPage l = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ "My list"
            ul_ $ mapM_ (li_ . toHtml) l 

main :: IO ()
main = do
    args <- getArgs
    if length args /= 2
    then do
        progName <- getProgName
        putStrLn $ "usage: " ++ progName ++ " <input dat> <output html>"
    else do
       f <- TIO.readFile $ head args
       renderToFile (last args) (myPage (T.lines f))

