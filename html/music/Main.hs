{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

data Name = Name T.Text deriving Show

instance FromRow Name where
    fromRow = Name <$> field 

main :: IO ()
main = do
    conn <- SQL.open "music.db"

    putStrLn "\n*** id et nom des artistes ***"
    -- TODO
    res <- SQL.query_ conn "SELECT * FROM artists" :: IO [(Int,T.Text)]
    print res

    putStrLn "\n*** id et nom de l'artiste 'Radiohead' ***"
    -- TODO
    res1 <- SQL.query_ conn "SELECT * FROM artists WHERE name='Radiohead'" :: IO [(Int,T.Text)]
    print res1
 
    putStrLn "\n*** tous les champs des titres dont le nom contient 'ust' et dont l'id > 1 ***"
    -- TODO
    res2 <- SQL.query_ conn "SELECT * FROM titles WHERE name like '%ust%' AND id>1" :: IO [(Int,Int,T.Text)]
    print res2

    putStrLn "\n*** noms des titres (en utilisant le type Name) ***"
    -- TODO
    res3 <- SQL.query_ conn "SELECT name FROM titles" :: IO [Name]
    print res3    

    putStrLn "\n*** noms des titres (en utilisant une liste de T.Text) ***"
    -- TODO
    res4 <- SQL.query_ conn "SELECT name FROM titles" :: IO [[T.Text]]
    print res4

    SQL.close conn

