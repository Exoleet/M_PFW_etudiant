{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Database.PostgreSQL.Simple as SQL
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)
import Lucid

data Name = Name T.Text deriving Show

data Music = Music
    { _title :: T.Text
    , _artist :: T.Text
    } deriving (Show)

instance FromRow Name where
    fromRow = Name <$> field 

instance FromRow Music where
    fromRow = Music <$> field <*> field

convertMusicToText :: Music -> T.Text
convertMusicToText m = T.concat [_artist m, " - " ,_title m]

myPage :: [Music] ->  Html()
myPage m = do
    doctype_
    html_ $ do
        meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ "My music"
            ul_ $ mapM_ (li_ . toHtml . convertMusicToText) m

main :: IO ()
main = do
    conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=music user=toto password='toto'"
    -- TODO
    res <- SQL.query_ conn "SELECT t.name,a.name FROM titles t, artists a where t.artist = a.id" :: IO [Music]

    print $ renderText (myPage res)
    renderToFile "index.html" (myPage res)
    SQL.close conn

