{ pkgs ? import <nixpkgs> {} }:

let 
  _music = pkgs.haskellPackages.callCabal2nix "music" ./. {};

  _drv = pkgs.stdenv.mkDerivation {
    name = "music-webserver";
    src = ./.;
    buildInputs = [ _music ];
    installPhase = ''
      mkdir $out
      cp ${_music}/bin/*.cgi $out/
      cp data/index.html $out/
      cp data/music.sql $out/
    '';
  };

in
if pkgs.lib.inNixShell then _music.env else _drv

