{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.IO as TIO
import qualified Data.Text as T
import           System.Environment (getArgs)

main :: IO ()
main = do
    n <- getArgs
    file <- readFile $ head n
    (mapM_ putStrLn) . lines $ file


